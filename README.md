# Trabajo Práctico Inicial Drivers

#Módulo Hola Mundo

Para poder realizar la carga de nuestro primer módulo debemos realixzar los siguientes pasos:

1- Creo una carpeta en donde se insertaremos dos archivos. 
2- Creamos el archivo miModulo.c y lo guardamos en la carpeta recién creada.
3- Creamos un archivo Makefile que se encargara del compilado y la preparación del módulo.
4- A través de la terminal ingresamos a la carpeta en donde se encuentran los archivos y ejecutamos el comando make.
5- Se generaron varios archivos pero el que nos interesa presenta la extensión .ko.
6- Cargamos el módulo a través del comando insmod de la siguiente manera:

	sudo insmod miModulo.ko
	
7- Para verificar que nuestro módulo se cargo correctamente verificamos el log del kernel en la terminal a través del comando "dmesg" y nos muestra la siguiente leyenda.

	[ 4111.511369] UNGS : Driver registrado
	
De esta manera queda verificado que nuestro primer módulo se ha cargado correctamente.

8- Para descargar el módulo realizamos lo siguiente:

	sudo rmmod mimodulo.ko
	
9- Verificamos nuevamente con el comando "dmesg" y obtenemos:

	[ 4599.128832] UNGS : Driver desregistrado
	
Confirmando nuestra operación.

# Dispositivo de Caracter

En este apartado elaboramos un módulo de kernel para un dispositivo de caracter que aplica un encriptado de tipo cesar a la información que se envía a dicho dispositivo.

Para establecer comunicación con nuestro dispositivo utilizaremos funciones provistas por la librería linux/fs.h a través de una estructura provista por GCC.

linux/fs.h
static int device_open (struct inode *, struct file *) ;
static int device_release (struct inode *, struct file *) ;
static ssize_t device_read (struct file *, char *, size_t , loff_t *) ;
static ssize_t device_write (struct file *, const char *, size_t , loff_t *);

Estructura gcc

static struct file_operations fops = {
 .read = device_read,
 .write = device_write,
 .open = device_open,
 .release = device_release
};

Con las funciones init_module y cleanup_module realizamos las llamaas a los correspondientes comandos isnmod y rmmod que montan y desmontan respectivamente nuestro módulo.

Con las funciones init_module y cleanup_module se realizan las llamadas a los correspondientes comandos isnmod y rmmod que montan y desmontan respectivamente nuestro módulo.
Para cifrar la información se incorporó el algoritmo cesar que se encarga de reemplazar cada letra de nuestro mensaje con la quinta que se encuentra a partir de esta, partiendo de un alfabeto 
algo desordenado. Al momento de leer y escribir datos entre los espacios del kernel y del usuario utilizamos las funciones put_user y get_user.

1- Creo una carpeta en donde se colocarán los archivos.
2- Creamos los archivos encriptadorCesar.c , Makefile y lo guardamos en la carpeta recién creada.
3- A través de la terminal ingresamos a la carpeta en donde se encuentran los archivos y ejecutamos el comando make.
4- Cargamos el módulo a través del comando insmod de la siguiente manera:

	sudo insmod encriptadorCesar.ko
	
5- Verificamos con el comando "dmesg" si se ha cargado correctamente y obtener el mayorNumber para crear una archivo y poder comunicarnos con el dispositivo.

[ 5703.529280] Me asignaron un MajorNumber  244.

6-Ahora que conocemos el número cremos un archivo para enviarle mensajes a través del comando mknod u para indicarle que cree un fichero especial sin buffer y los canales principales 
y segundarios.

	sudo mknod /dev/encriptadorCesar u 244 0
	
7- Para enviarle datos debemos tener los permisos necesarios pues es un archivo protegido, procedemos a habilitar dichos permisos.

	sudo chmod 777 /dev/encriptadorCesar
	
8- Para verificar el funcionamiento probamos el dispositivo de la sih¡guiente manera:

	le enviamos un mensaje.
	
	echo "Maximiliano" > /dev/encriptadorCesar
	
	consultamos la información del dispositivo.
	
	Ftqbfbebtgh
	
	Y se observa que efectivamente la información se encuentra encriptada.
 

