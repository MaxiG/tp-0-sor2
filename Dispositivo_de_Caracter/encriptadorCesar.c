#include <linux/string.h> 
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h> 
#include <linux/ctype.h> 

static int device_open (struct inode *, struct file *) ;
static int device_release (struct inode *, struct file *) ;
static ssize_t device_read (struct file *, char *, size_t , loff_t *) ;
static ssize_t device_write (struct file *, const char *, size_t , loff_t *);

/* Nombre del driver */
#define driver "Locker"
/* Tamaño de leyenda del dispositivo */
#define bufferDispositivo  1000

/* Declaracion de variables golbales que afectan a todo el archivo.*/
static int majorNumber;
static int estadoDispositivo = 0;
static char letras[bufferDispositivo];
static char *p_letras;
static char *p_letrasAx;
static int vueltas = 5;

/* Variables para la encriptacion */

#define abcLetras 26
#define primerLetraMinus 97
#define primerLetraMayus 65

const char *minusculas = "opqrstuvwxyzabcdefghijklmn",
					*mayusculas = "JKLMNOPQRSTUABCDEFGHIVWXYZ";		
								
/* Recibe un mensaje a cifradoCesar y un búfer en donde pondrá el resultado, así como */
// las rotaciones que debe dar
void cifradoCesar(char *info, char *crypto);

// Obtener el valor entero de un carácter:
int ord(char c);

/*Una instancia de struct file_operations que contiene punteros a funciones que se utilizan para implementar lectura, escritura en el dispositivo.*/
static struct file_operations fops = {
 .read = device_read,
 .write = device_write,
 .open = device_open,
 .release = device_release
};

/*funcion para cargar el modulo en el kernel*/
int init_module ( void ){ 
    /* registramos el dispositvo asignando el mayor number y las operaciones que se pueden realizar */
    majorNumber = register_chrdev(0, driver, &fops);
    if (majorNumber < 0) {
        printk(KERN_ALERT  "No se pudo registrar el Dispositivo debido a un error, revise las instrucciones %d\n", majorNumber);
        return majorNumber;
    }
            printk(KERN_INFO  "Me asignaron un MajorNumber %d. Para comunicarme con\n", majorNumber);
            printk(KERN_INFO  "El driver, crea un dev file con\n");
            printk(KERN_INFO   "'mknod /dev/%s c %d 0' .\n", driver, majorNumber);
            printk(KERN_INFO  "Pruebe con varios números menores. Intenta con cat y echo\n");
            printk(KERN_INFO  "El archivo de dispositivo.\n");
            printk(KERN_INFO  "Retire el archivo del dispositivo y el módulo cuando termine.\n");
    return 0;
}

/* Esta función se llama cuando el módulo se descarga */
void cleanup_module ( void ){
    unregister_chrdev(majorNumber, driver);
    printk ( KERN_INFO "Driver desregistrado exitosamente \n") ;
}

/*Esta funcion se llama cuando un proceso intenta abrir el archivo del dispositivo*/
static int device_open(struct inode *inode, struct file *file){
    if (estadoDispositivo) return -EBUSY;
    estadoDispositivo++;
    try_module_get(THIS_MODULE);
    return 0;
}

/* Esta funcion se llama cuando un proceso cierra el archivo del dispositivo.*/
static int device_release(struct inode *inode, struct file *file){
    estadoDispositivo--; 
    module_put(THIS_MODULE);
    return 0;
}

/* Esta funcion se llama cuando un proceso, que ya abrió el archivo dev, intenta leerlo. */
static ssize_t device_read(struct file *filp, char *buffer, size_t length, loff_t * offset){
    /*numero de bytes escritos en el buffer*/
    int bytes_read = 0;
    /*Si estamos al final del mensaje, devuelve 0 que significa fin de archivo.*/
    if (*p_letras == 0)return 0;
    /*poner los datos en buffer*/
    while (length && *p_letras) {
		/*El búfer está en el segmento de datos de usuario, tenemos que usar
		put_user que copia los datos del segmento de datos del kernel a
		el segmento de datos del usuario.*/
        put_user(*(p_letras++), buffer++);
        length--;
        bytes_read++;
    }
    /*La mayoría de las funciones de lectura devuelven el número de bytes puestos en el buffer */
    return bytes_read;
}

/* Esta funcion se llama cuando un proceso escribe en el dev file. */
static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t * off){
        int i;
        for (i = 0; i < len && i < bufferDispositivo; i++){
            get_user(letras[i], buff + i);
        }
        letras[len] = '\0'; 
        p_letrasAx = letras;
        
        cifradoCesar(letras, p_letrasAx);

        p_letras = p_letrasAx;
        printk(KERN_ALERT "La Encriptacion se ha realizado con exito, su informacion se encuentra segura.\n");
        return i;
}
void cifradoCesar(char *info, char *crypto) {
  /*se realiza un recorrido del mensaje*/
  int i = 0;
  while (info[i]) {
    char letraActual = info[i];
    int posInicial = ord(letraActual);
    if (!isalpha(letraActual)) {
      crypto[i] = letraActual;
      /*se aumenta la i para seguir con la iteracion*/
      i++;
      continue; 
    }
    if (isupper(letraActual)) {
      crypto[i] =
          mayusculas[(posInicial - primerLetraMayus +
                              vueltas) %
                             abcLetras];
    } else {

      crypto[i] =
          minusculas[(posInicial - primerLetraMinus +
                              vueltas) %
                             abcLetras];
    }
    i++;
  }
}

int ord(char c) { return (int)c; }


MODULE_LICENSE("GPL");

MODULE_DESCRIPTION("Driver Encriptador de Informacion");

MODULE_AUTHOR("Maximiliano Gamarra");

